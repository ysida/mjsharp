﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace mjsharp {
	public enum HaiType {
		Manz, Souz, Pinz, Sangen, Fu
	}

	/// <summary>
	/// 牌クラス.
	/// </summary>
	public class Hai {
		public Hai (int s, HaiType t, int no) {
			serial = s;
			haiType = t;
			haiNo = no;
		}
		public readonly int serial;
		public readonly HaiType haiType;
		public readonly int haiNo;
		public override string ToString () {
			return string.Format ("{0}:{1},{2}", serial, haiType, haiNo);
		}

		// 書いてはみたもののきっと使わない
		static Hai nullHai = null;
		static public Hai NullHai () {
			if (nullHai == null) {
				nullHai = new Hai (-1, (HaiType)(-1), -1);
			}
			return nullHai;
		}
		public bool IsNullHai () {
			return (serial < 1);
		}

		/// <summary>
		/// 同じ牌種かをチェック.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public bool eq (Hai other) {
			return (this.haiType == other.haiType && this.haiNo == other.haiNo && !this.IsNullHai () && !other.IsNullHai ());
		}

	}

	/// <summary>
	/// 牌生成.
	/// </summary>
	public class HaiGeneattor {
		static public List<Hai> Run () {
			List<Hai> ret = new List<Hai> ();

			int serial = 1;
			for (int ii = 1; ii < 10; ii++) {
				for (int ik = 0; ik < 4; ik++) {
					ret.Add (new Hai (serial++, HaiType.Manz, ii));
				}
			}
			for (int ii = 1; ii < 10; ii++) {
				for (int ik = 0; ik < 4; ik++) {
					ret.Add (new Hai (serial++, HaiType.Pinz, ii));
				}
			}
			for (int ii = 1; ii < 10; ii++) {
				for (int ik = 0; ik < 4; ik++) {
					ret.Add (new Hai (serial++, HaiType.Souz, ii));
				}
			}
			for (int ii = 1; ii < 5; ii++) {
				for (int ik = 0; ik < 4; ik++) {
					ret.Add (new Hai (serial++, HaiType.Sangen, ii));
				}
			}
			for (int ii = 1; ii < 5; ii++) {
				for (int ik = 0; ik < 4; ik++) {
					ret.Add (new Hai (serial++, HaiType.Fu, ii));
				}
			}
			return ret;
		}
	}

	public class CreateHaiCombination {
		/// <summary>
		/// 頭とそれ以外のリストのリストを作る.
		/// </summary>
		public List<ParseHai> CreateHeadList (IList<Hai> l) {
			// コピー。元リスト残す.
			IList < Hai > list = new List<Hai> (l);
			List<ParseHai> phList = new List<ParseHai> ();
            foreach (var h in list) {
				var c = ParseHai.CreateParseHai (h, list);
				if (c != null) {
					phList.Add (c);
				}
            }
			// TODO 頭１つにつき２つのリストができてしまう.
			return phList;
		}


		public class ParseHai {
			public static ParseHai CreateParseHai (Hai h, IList<Hai> l) {
				IList<Hai> head = new List<Hai>();
				IList<Hai> body = new List<Hai> ();
				IList<Hai> unKnown = new List<Hai> (l);

				var rh1 = l.ToList ().RemoveHai (h.haiType, h.haiNo);
				if (rh1 == null) {
					return null;
				}
				var rh2 = l.ToList ().RemoveHai (h.haiType, h.haiNo);
				if (rh2 == null) {
					return null;
				}
				head.Add (rh1);
				head.Add (rh2);
				return new ParseHai (head, body, unKnown);
			}

			private ParseHai (IList<Hai> head,IList<Hai> body, IList<Hai> unKnown) {
				this.head = head;

				this.body = body;
				this.unKnown = unKnown;
			}
            public IList<Hai> head;
			public IList<Hai> body;
			public IList<Hai> unKnown;
		}


	}


	/// <summary>
	/// 拡張メソッド.
	/// </summary>
	public static class Extention {

		/// <summary>
		/// 非破壊　リストのシャッフル用拡張メソッド.
		/// 元リストの変更は行わない.
		/// usage : list.shuffle<T> ()
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <returns></returns>
		public static List<T> Shuffle<T>(this List<T> list) {
			var ret = list.OrderBy (h => Guid.NewGuid ()).ToList ();
			return ret;
		}

		/// <summary>
		/// 非破壊　牌リストのソート.
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static List<Hai> HaiSort (this List<Hai> list) {
			var ret = list.OrderBy (h => h.haiType).ThenBy (h => h.haiNo).ToList ();
			return ret;
		}

		/// <summary>
		/// 破壊的　リストから指定個数分取得する.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="num"></param>
		/// <returns></returns>
		public static IList<Hai> RemoveHai (this List<Hai> list, int num) {
			IList<Hai> ret = new List<Hai> ();
			for (int ii=0;ii< num; ii++) {
				if (list.Count == 0) { break; }
				ret.Add (list[0]);
				list.RemoveAt (0);
			}
			return ret;
		}

		/// <summary>
		/// 破壊的　指定した牌を１つ取り除いて戻り値として返す.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="type"></param>
		/// <param name="no"></param>
		/// <returns></returns>
		public static Hai RemoveHai (this List<Hai> list, HaiType type, int no) {
			return RemoveHaiUtil (list, type, no);
		}

		/// <summary>
		/// 破壊的 指定した牌をリストから削除.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="type"></param>
		/// <param name="no"></param>
		/// <returns></returns>
		private static Hai RemoveHaiUtil (IList<Hai> list, HaiType type, int no) {
			var ret = list.ToList().Find (h => h.haiType == type && h.haiNo == no);
			if (ret != null) {
				list.Remove (ret);
			}
			return ret;
		}

		/// <summary>
		/// 破壊的　指定した牌姿文字列を牌リストから抜き出す.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="strList"></param>
		/// <returns></returns>
		public static List<Hai> RemoveHaiStringList (this List<Hai> list, string strList) {
			List<Hai> ret = new List<Hai> ();
			for (int ii = 0; ii < strList.Length; ii += 2) {
				var type = strList[ii];
				var no = strList[ii + 1];
				ret.Add (RemoveHaiUtil (list, CherToHaiType (type), int.Parse (no.ToString ())));
			}
			return ret;
		}

		private static HaiType CherToHaiType (char c) {
			switch (c) {
			case 'M':
				return HaiType.Manz;
			case 'S':
				return HaiType.Souz;
			case 'P':
				return HaiType.Pinz;
			case 'G':
				return HaiType.Sangen;
			case 'F':
				return HaiType.Fu;
			}
			// エラー.
			return HaiType.Manz;
		}

		/// <summary>
		/// 破壊的　一番先頭に近い刻子を取得し元リストから削除する.
		/// ※要ソート.
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static List<Hai> RemoveKotu (this List<Hai> list) {
			return RemoveSameHaiUtil (list, 3);
		}

		/// <summary>
		/// 破壊的　一番先頭に近い対子を取得し元リストから削除する.
		/// ※要ソート.
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static List<Hai> RemoveToitu (this List<Hai> list) {
			return RemoveSameHaiUtil (list, 2);
		}

		/// <summary>
		/// リストの先頭に一番近い同じ牌がｎ個ある牌を検索して返す(元リストの変更はしない).
		/// 対子、刻子取得用
		/// ※要ソート.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="n"></param>
		/// <returns></returns>
		private static List<Hai> RemoveSameHaiUtil (List<Hai> list, int n) {
			List<Hai> ret = new List<Hai> ();
			Hai sameHai = SearchSameHaiUtil (list, n);
			if (sameHai == null) { return ret; }
			for (int ii = 0; ii < n; ii++) {
				ret.Add (RemoveHaiUtil (list, sameHai.haiType, sameHai.haiNo));
			}
			return ret;
		}

		/// <summary>
		/// リストの先頭に一番近いｎ個ある同じ牌を検索して返す.
		/// ※要ソート.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="n"></param>
		/// <returns></returns>
		private static Hai SearchSameHaiUtil (List<Hai> list, int n) {
			Hai wkHai = list[0];
			int sameCount = 1;
			for (int ii = 1; ii < list.Count; ii++) {
				if (wkHai.eq (list[ii])) {
					sameCount++;
					if (sameCount == n) {
						return wkHai;
					}
				} else {
					sameCount = 1;
					wkHai = list[ii];
				}
			}
			return null;
		}

		/// <summary>
		/// 非破壊　牌を牌種ごとに分ける拡張メソッド.
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static IDictionary<HaiType, IList<Hai>> DispatchHaiType (this IList<Hai> list) {
			IDictionary<HaiType, IList<Hai>> ret = new Dictionary<HaiType, IList<Hai>> ();
			foreach (var h in list) {
				if (!ret.ContainsKey (h.haiType)) {
					ret.Add (h.haiType, new List<Hai> ());
				}
				ret[h.haiType].Add (h);
			}
			return ret;
		}

		/// <summary>
		/// 破壊的　順子を取り出して元リストからは削除する.
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static IList<Hai> RemoveShuntu (this IList<Hai> list) {
			List<Hai> ret = new List<Hai> ();
			IList<Hai> shuntu = SearchShuntuUtil (list);
			if (shuntu.Count == 0) { return ret; }
			foreach (var h in shuntu) {
				ret.Add (RemoveHaiUtil (list, h.haiType, h.haiNo));
			}
			return ret;
		}

		/// <summary>
		/// 非破壊　リストから順子を検索.
		/// ※要ソート済、要同一牌種のリスト.
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static IList<Hai> SearchShuntuUtil (IList<Hai> list) {
			IList<Hai> shuntu = new List<Hai> ();

			for (int ii = 0; ii < list.Count (); ii++) {
				if (shuntu.Count () == 0) {
					shuntu.Add (list[ii]);
				} else if (shuntu[shuntu.Count () - 1].haiNo + 1 == list[ii].haiNo) {
					shuntu.Add (list[ii]);
				} else {
					if (list[ii].haiNo >= 8) {
						// 8以上から開始だと順子を作れない.
						shuntu.Clear ();
						break;
					}
					shuntu.Clear ();
					shuntu.Add (list[ii]);
				}
				if (shuntu.Count () >= 3) {
					break;
				}
			}
			return shuntu;
		}

		/// <summary>
		/// 同じ牌の並びかを判定.
		/// ※ソート済であること.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="other"></param>
		/// <returns></returns>
		public static bool Eq (this List<Hai> list, List<Hai> other) {
			if (list.Count != other.Count) { return false; }
			for (int ii = 0; ii < list.Count; ii++) {
				if (!list[ii].eq (other[ii])) { return false; }
			}
			return true;
		}

		/// <summary>
		/// 非破壊 対子のリスト作成.
		/// 引数のソート前提.
		/// </summary>
		/// <param name="list"></param>
		/// <returns></returns>
		public static List<List<Hai>> CreateToituList (this IList<Hai> list) {
			List<List<Hai>> ret = new List<List<Hai>> ();
            Hai prev = null;
			bool skip = false;
			for (int ii = 0; ii < list.Count; ii++){
				Hai hai = list[ii];
				if (skip) {
					// 対子成立後同じ牌をスキップ中.
					if (!hai.eq (prev)) {
						skip = false;
					}
				} else if ((prev != null) && (hai.eq (prev))) {
					// 前の牌と同じか？（対子成立？）.
					List<Hai> toitu = new List<Hai> () { prev, hai };
					ret.Add (toitu);
					skip = true;
				}
				prev = hai;
			}
			return ret;
		}

		/// <summary>
		/// リスト用のToString()拡張メソッド.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <returns></returns>
		public static string ListToString<T>(this IList<T> list) {
			string ret = "";
			foreach (var h in list) {
				ret = ret + h.ToString () + "\n";
			}
			return ret;
		}

		/// <summary>
		/// IDictionary<HaiType,List<T>>用のToString拡張メソッド.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dic"></param>
		/// <returns></returns>
		public static string DictionaryToString<T>(this IDictionary<HaiType, IList<T>> dic) {
			string ret = "";
			foreach (var pair in dic) {
				ret = ret + pair.Key.ToString () + ":";
				foreach (var h in pair.Value) {
					ret = ret + h.ToString () + " /";
				}
				ret = ret + "\n";
			}
			return ret;
		}

	}

}
